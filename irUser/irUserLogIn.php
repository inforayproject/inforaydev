<?php
/**
* Date: Nov 28th 2016
  Author: Arun Billur
*/
require_once(realpath(__DIR__. DIRECTORY_SEPARATOR . '..')."/irUtility/common.php"); 
class IrUserLogIn extends Common
{
	function __construct(){
		session_destroy();   
		session_start();
		//$con = new connection();
		$username = $this->cleanInputs($_POST['username']);
		$password = $this->cleanInputs($_POST['password']);
		$loggedInIp = $_SERVER['REMOTE_ADDR'];
		$browser = $_SERVER['HTTP_USER_AGENT'];
		$date = date('Y-m-d H:i:s');	
		$validateUsernameQuery = "SELECT user_auth,user_token from irUserLogIn where username = "."'".$username."'"."";
		$validateUsername = $this->getNumRows($validateUsernameQuery);
		if(!$validateUsername){
			$this->errorMesaage('Please Enter the valid username');
		}
		$rlt = $this->execute($validateUsernameQuery);
		while ($result = $rlt->fetch_assoc()){
			$user_auth = $result['user_auth'];
			$user_token = $result['user_token'];
		}
		$user_password=hash("sha256",$password.strrev($user_auth).$user_token);
		$validatePassword = "SELECT userlogInId,username,status,role from irUserLogIn where username = "."'".$username."'"." and user_password = "."'".$user_password."'"."";
		if($this->getNumRows($validatePassword)){
			$validatePassword = $this->execute($validatePassword);
			while($rlt = $validatePassword->fetch_assoc()){
			if(strcasecmp($rlt['status'],'active')!=0){
				$this->errorMesaage('Your are not an authorised user, please contact admin.');
			}
			else{
				$this->storeLoginHistory($rlt['userlogInId'],$loggedInIp,$browser,$date,'user');
				$_SESSION["logInId"] = $rlt['userlogInId'];
				$_SESSION["username"] = $rlt['username'];
				$_SESSION["role"] = $rlt['role'];
				$this->response($this->json($rlt),200);
			}
			}
		}
		else{
			$this->errorMesaage('Please Enter the valid password');
		}
		
	}
}
$obj = new irUserLogIn();