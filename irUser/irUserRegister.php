<?php
/**
* Date: 3rd Nov 2016
  Author: Arun Billur
*/
//require_once(realpath(__DIR__. DIRECTORY_SEPARATOR . '..')."/irUtility/connection.php"); 
require_once(realpath(__DIR__. DIRECTORY_SEPARATOR . '..')."/irUtility/common.php"); 
class IrUserRegister extends Common
{
	function __construct(){
		//$con = new connection();
		$obj = $_POST;
		$userName = str_replace("'", "",$this->cleanInputs($obj['username']));
		$email = $this->cleanInputs($obj['email']);
		//$companyName = str_replace("'", "",$this->cleanInputs($obj['companyName']));
		$address = str_replace("'", "",$this->cleanInputs($obj['name']));
		$mobile = $this->cleanInputs($obj['mobile']);
		$password = $this->cleanInputs($obj['password']);
		$confirmPassword = $this->cleanInputs($obj['confirmPassword']);
		$adminId = 1;//$this->cleanInputs($obj['adminId']);
		$role = 'admin';//$this->cleanInputs($obj['role']);
		$date=date('Y-m-d H:i:s',strtotime('+5 hour +30 minutes'));

		$generatedPassword = $this->generatedPassword($password);
		$sql = "INSERT INTO irUserLogIn (username,userMobile,userEmail,user_auth,user_token,user_password,userAddress,adminId,role,create_ts) VALUES ("."'".$userName."'".","."'".$mobile."'".","."'".$email."'".","."'".$generatedPassword['auth']."'".","."'".$generatedPassword['token']."'".","."'".$generatedPassword['password']."'".","."'".$address."'".","."'".$adminId."'".","."'".$role."'".","."'".$date."'".")";		
		$execute = $this->execute($sql);		
		if($execute){
			$this->successMessage("Successfully Registered.");
			//$this->sendMail($userName,$email,$mobile,$date);
		}
		else{
			$this->errorMesaage("Something Went Wrong, Please try again.");
		}
	}

	public function generatedPassword($password){
		$data['auth'] = md5(uniqid(mt_rand(), true));
		$data['token'] = bin2hex(openssl_random_pseudo_bytes(24));
		$data['password'] = hash("sha256",$password.strrev($data['auth']).$data['token']);
		return $data;
	}

	public function checkUserNameExist($username){
		$sql = "SELECT username FROM irUserLogIn WHERE username = "."'".$username."'"."";
		if(!$this->getNumRows($sql)){
			return true;
		} 
		else{
			$this->errorMesaage("Username is already exists..!");
		}
	}

	public function checkNumberExist($mobile){
		$sql = "SELECT userMobile FROM irUserLogIn WHERE userMobile = "."'".$mobile."'"."";
		if(!$this->getNumRows($sql)){
			return true;
		} 
		else{
			$this->errorMesaage("Mobile number is already exists..!");
		}
	}

	public function checkEmailExist($email){
		$sql = "SELECT userEmail FROM irUserLogIn WHERE userEmail = "."'".$email."'"."";
		if(!$this->getNumRows($sql)){
			return true;
		}
		else{
			$this->errorMesaage("Email is already exists..!");
		}
	}

}
$obj = new irUserRegister();