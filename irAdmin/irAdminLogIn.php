<?php
/**
* Date: Nov 28th 2016
  Author: Arun Billur
*/
require_once(realpath(__DIR__. DIRECTORY_SEPARATOR . '..')."/irUtility/common.php");
class IrAdminLogIn extends Common
{
	//Admin LogIn Details
	function __construct(){	
		$username = $this->cleanInputs($_POST['username']);
		$password = $this->cleanInputs($_POST['password']);
		$loggedInIp = $_SERVER['REMOTE_ADDR'];
		$browser = $_SERVER['HTTP_USER_AGENT'];
		$date = date('Y-m-d H:i:s');
		
		$validateUsername = "SELECT admin_auth,admin_token from irAdminLogIn where adminusername = "."'".$username."'"." and status = 'active'";
		$validateUsername = $this->getNumRows($validateUsername);
		if(!$validateUsername){
			$this->errorMesaage('Please Enter the valid username');
		}
		while($result=mysqli_fetch_assoc($validateUsername)){
			$user_auth = $result['admin_auth'];
			$user_token = $result['admin_token'];
		}
		$user_password=hash("sha256",$password.strrev($user_auth));
		$validatePassword = "SELECT adminId,adminusername,status,role from irAdminLogIn where username = "."'".$username."'"." and user_password = "."'".$user_password."'"."";
		if($this->getNumRows($validatePassword)){
			while ($rlt=mysqli_fetch_assoc($validatePassword)){
			if(strcasecmp($rlt['status'],'active')!=0){
				$this->errorMesaage('Your are not an authorised user, please contact admin.');
			}
			else{
				$this->storeLoginHistory($rlt['adminId'],$loggedInIp,$browser,$date,$rlt['role']);
				$this->response($this->json($rlt),200);
			}
			}
		}
		else{
			$this->errorMesaage('Please Enter the valid password');
		}
	}
}
$obj = new irUserLogIn();