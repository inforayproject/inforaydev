<?php
/* 
  Date: 19th Dec 2016
  Author: Arun Billur
*/
require_once(realpath(__DIR__. DIRECTORY_SEPARATOR . '..')."/irUtility/common.php"); 
class IrUserStatus extends common
{
	//Update the User Status and send sms to customer that use account has been acivated
	public function updateUserStatus(){
		$logInId = $this->cleanInputs($_POST['logInId']);
		$status = $this->cleanInputs($_POST['status']);
		$sql = $this->con->query("UPDATE irUserLogIn SET status = "."'".$status."'"." WHERE userlogInId = ".$logInId."");
		if($sql){
			//Send Sms to user for active status
			$this->SuccessMessage('Updated Successfully');
		}
		else{
			$this->errorMesaage('Something went wrong,Please try again');
		}
	}

	//To get all the user status
	public function getUserLogInStatus(){
	  	$adminId = $this->cleanInputs(SESSIONID);
	  	$sql = $this->con->query("SELECT userlogInId,username FROM irUserLogIn WHERE adminId = ".$adminId."");
	  	while($rlt = $sql->fetch_assoc()){
	  		$this->response($this->json($rlt),200);
	  	}
	}
}