<?php
/**
* Date: 28th Nov 2016
  Author: Arun Billur
*/
class irSenderId extends AnotherClass
{
	function add_senderid($nsnid,$parent)
	{
		/*New validation from block senderid added by Sidhartha*/
		$block_query = "SELECT * FROM blocked_sender_ids WHERE blocked_sender_ids='".$nsnid."'";
		$blokerid = $this->get_row($block_query);

		if($this->exists($block_query)){
			$db_status = 'Sender ID Blocked@#@st-success';
		}
		/*Close*/
		else{
        //echo $block_status;exit;	
		$query = "SELECT * FROM irSenderIds WHERE irLogInId='".$_SESSION[SENLOGIN]['lo_id']."' AND BINARY irSenderId='".$nsnid."'";
		if($this->exists($query))
		{
			$snederid = $this->get_row($query);
			if($snederid['st_sts'] == '4')
			{
				$update = array('st_sts'=>'3','irSenderId'=>$nsnid);
				$where = array('irLogInId'=>$_SESSION[SENLOGIN]['lo_id'],'irSenderId'=>$nsnid);
				$this->update('irSenderIds',$update,$where);
				$db_status = 'Sender ID added@#@st-success';
			}
			else
			{
				$db_status = 'Sender ID already exists@#@st-info';
			}
		}
		else
		{
			$names = array('irLogInId'=>$_SESSION[SENLOGIN]['lo_id'],'irSenderId'=>$nsnid,'st_sts'=>'3');
			$this->insert('irSenderIds',$names);
			$db_status = 'Sender ID added@#@st-success';
		}
	   }
		return $db_status;
	}
	function check_senderid_exe($senderid)
	{
		$query = "SELECT * FROM irSenderIds WHERE BINARY irSenderId='".$senderid."'";
		if($this->exists($query))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	function select_senderids()
	{
		$query = "SELECT * FROM irSenderIds WHERE irLogInId='".$_SESSION[SENLOGIN]['lo_id']."' AND st_sts<='3' ORDER BY st_dlf DESC";
		return $this->get_results($query);
		exit();
	}
	function delete_senderid($senderid)
	{
		$update = array('st_sts'=>'4');
		$where = array('irLogInId'=>$_SESSION[SENLOGIN]['lo_id'],'sd_id'=>$senderid);
		$this->update('irSenderIds',$update,$where);
	}
}