<?php
/*
* Author 	: ravi@inforay.co
* Date 		: 23rd Nov 2016
* Purpose 	: To blacklist Numbers With respect to a specific user. 
*/

error_reporting(E_ALL);
set_time_limit(0);

/*
* 	To add the common functinalities.
*/  

require_once '../irUtility/Common.php';

/*
*	Class definition: To encapsulate the blacklist variables and functions together in
*	order to use these functionalities for blacklisting of phone numbers respective to
* 	sepecific users. i.e to prevent sending of messages to few numbers on the basis of
* 	an user requirement.
*
*	var $userRole : Private variable accesible within the class scope only.
*
*	__construct($role) : callling the constructor the of the class & base class and 
* 	 passing $role as param to the constuctor the set the Role of the  user.
*	   
*/


class blacklistClass extends Common
{
	private $userRole;

	public function __construct($role)
	{
		$this->userRole = $role;
		parent::__construct();
	}

	/*
	*	blacklistSingleNumber : To blacklist a Number on the basis of user and userrole.
	*	Required Parms: 
	*	$userLoginid : loginid of the user.
	* 	$creditType  : To blacklist according to the promotional | Transactiona | OTP |	
	*	Senderid route basis.
	*	$num : Number which has to be blacklisted.
	*/

	public function blacklistSingleNumber($userLoginid,$creditType,$num)
	{
		$this->setDate();
		
		$userLoginid = $this->cleanInputs($userLoginid);
		$creditType  = $this->cleanInputs($creditType);
		$num  		 = $this->cleanInputs($num);

		if($this->userRole == 'Admin')
		{
			$columns  = array();
			$colArray = array('BLadminId','BLCTID','BLnumbers');
			$valArray = array($userLoginid,$creditType,$num);

			$query = $this->where($columns,TableNames['blacklist'],$colArray,$valArray);
			// echo $query;

			$result = $this->exists($query);
			$row 	= $this->getSingleRow($query);

			if($result == true)
			{
				$query = "UPDATE"." ".TableNames['blacklist']." "."SET BLadminId = '$userLoginid', BLCTID = '$creditType', BLnumbers = '$num', BLupdatedTime = '$this->today' WHERE BLID =".$row['BLID'];

				// echo "$query";		
				$res = $this->execute($query);

				return ($res == TRUE) ? $this->successMessage("updated succesfully") : $this->errorMesaage("Server is busy Unable to process the request now. Try again later"); 			
			}
			else
			{
				$colArray = array('BLadminId','BLCTID','BLnumbers','BLcreatedTime');
				$valArray = array($userLoginid,$creditType,$num,$this->today);

				$res = $this->Insert($colArray,$valArray,TableNames['blacklist']);

				return ($res == TRUE) ? $this->successMessage("updated succesfully") : $this->errorMesaage("Server is busy Unable to process the request now. Try again later"); 				
			}
		}
		else
		{
			$columns  = array();
			$colArray = array('BLuserlogInId','BLCTID','BLnumbers');
			$valArray = array($userLoginid,$creditType,$num);

			$query = $this->where($columns,TableNames['blacklist'],$colArray,$valArray);
			// echo $query;

			$result = $this->exists($query);
			$row 	= $this->getSingleRow($query);

			if($result == true)
			{
				$query = "UPDATE"." ".TableNames['blacklist']." "."SET BLuserlogInId = '$userLoginid', BLCTID = '$creditType', BLnumbers = '$num', BLupdatedTime = '$this->today' WHERE BLID =".$row['BLID'];

				// echo "$query";		
				$res = $this->execute($query);

				return ($res == TRUE) ? $this->successMessage("updated succesfully") : $this->errorMesaage("Server is busy Unable to process the request now. Try again later"); 			
			}
			else
			{
				$colArray = array('BLuserlogInId','BLCTID','BLnumbers','BLcreatedTime');
				$valArray = array($userLoginid,$creditType,$num,$this->today);

				$res = $this->Insert($colArray,$valArray,TableNames['blacklist']);

				return ($res == TRUE) ? $this->successMessage("Inserted succesfully") : $this->errorMesaage("Server is busy Unable to process the request now. Try again later");				
			}		
		}			
	}

	/*
	* 	blacklistBulkNumbers : To read bulk Numbers from file and blacklist for the user.
	*	$filepath : To pass the path of the file from which numbers has to be 			
	*	blacklisted.
	*/

	public function blacklistBulkNumbers($userLoginid,$creditType,$filepath)
	{		
		$this->setDate();

		$userLoginid = $this->cleanInputs($userLoginid);
		$creditType  = $this->cleanInputs($creditType);
		$table 		 = TableNames['blacklist'];
		
		$file 		 = fopen("$filepath","r");

		$flag 		 = true;

		if($this->userRole == 'Admin')
		{
			while (($line = fgetcsv($file)) !== FALSE)
			{
				if($flag)
				{
					$flag = false;
					continue;
				}
				foreach($line as $num)
				{					
					$columns  	= array();
					$colArray 	= array('BLadminId','BLCTID','BLnumbers');
					$valArray 	= array($userLoginid,$creditType,$num);

					$SelectQry 	= $this->where($columns,$table,$colArray,$valArray);

					$result 	= $this->exists($SelectQry);
					$row 		= $this->getSingleRow($SelectQry);

					if($result == true)
					{
						$query = "UPDATE"." ".TableNames['blacklist']." "."SET BLadminId = '$userLoginid', BLCTID = '$creditType', BLnumbers = '$num', BLupdatedTime = '$this->today' WHERE BLID =".$row['BLID'];

						// echo "$query";		
						$res = $this->execute($query);

						return ($res == TRUE) ? $this->successMessage("updated succesfully") : $this->errorMesaage("Server is busy Unable to process the request now. Try again later");					
					}
					else
					{
						$colArray = array('BLadminId','BLCTID','BLnumbers','BLcreatedTime');
						$valArray = array($userLoginid,$creditType,$num,$this->today);

						$query 	  = $this->Insert($colArray,$valArray,$table);
						
						$res 	  = $this->execute($query);
						// echo "$query";

						return ($res == TRUE) ? $this->successMessage("Inserted succesfully") : $this->errorMesaage("Server is busy Unable to process the request now. Try again later");
					}									
				}
			}
		}
		else
		{
			while (($line = fgetcsv($file)) !== FALSE)
			{
				if($flag)
				{
					$flag = false;
					continue;
				}
				foreach($line as $num)
				{
					$columns  	= array();
					$colArray 	= array('BLuserlogInId','BLCTID','BLnumbers');
					$valArray 	= array($userLoginid,$creditType,$num);

					$SelectQry 	= $this->where($columns,$table,$colArray,$valArray);

					$result 	= $this->exists($SelectQry);
					$row 		= $this->getSingleRow($SelectQry);

					if($result == true)
					{
						$query = "UPDATE"." ".TableNames['blacklist']." "."SET BLuserlogInId = '$userLoginid', BLCTID = '$creditType', BLnumbers = '$num', BLupdatedTime = '$this->today' WHERE BLID =".$row['BLID'];

						// echo "$query";		
						$res = $this->execute($query);

						return ($res == TRUE) ? $this->successMessage("updated succesfully") : $this->errorMesaage("Server is busy Unable to process the request now. Try again later");					
					}
					else
					{
						$colArray = array('BLuserlogInId','BLCTID','BLnumbers','BLcreatedTime');
						$valArray = array($userLoginid,$creditType,$num,$this->today);

						$query 	  = $this->Insert($colArray,$valArray,$table);
						
						$res 	  = $this->execute($query);

						return ($res == TRUE) ? $this->successMessage("updated succesfully") : $this->errorMesaage("Server is busy Unable to process the request now. Try again later");	
					}									
				}
			}
		}
		
		fclose($file);
		exit();
	}

	/*
	* getEntireBlData will return all the table colum | row present in the blacklist
	* table. It can be used to display all the blacklisted data.
	*/

	public function getEntireBlData()
	{
		$query = "SELECT a.*,b.* FROM"." ".TableNames['User']." "."a INNER JOIN"." ".TableNames['blacklist']." "."b ON a.userlogInId = b.BLuserlogInId";

		$data 	= $this->execute($query);

		$result = $this->fetchArray($data);

		return $result;
	}

	/*
	* getUserWiseBlData: it can be used to show user specific data. 
	* param $userlogInId is mandatory to pass.
	*/

	public function getUserWiseBlData($userlogInId)
	{
		$query = "SELECT a.*,b.* FROM"." ".TableNames['User']." "."a INNER JOIN"." ".TableNames['blacklist']." "."b ON a.userlogInId = b.BLuserlogInId WHERE a.userLoginid = '$userlogInId'";

		// echo $query;
		$data = $this->execute($query);

		$result = $this->fetchArray($data);
		return $result;		
	}
}
