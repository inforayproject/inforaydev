<?php
/*
* Author 	: ravi@inforay.co
* Date 		: 4th Dec 2016
* Purpose 	: To handle group functionalities. 
*/

error_reporting(E_ALL);
set_time_limit(0);

require_once '../irUtility/Common.php';

class groupClass extends Common
{	
	private $userRole;

	public function __construct($role)
	{
		$this->userRole = $role;		 
		parent::__construct();
	}

	/*
	* createGroup : it can be used to create a new group for user account.
	* parms $userlogInId : it is mandatory to pass,
	* $groupName : give a new group Name to create,
	* $numArray  : pass the number array of users which you want to include in this
	* 			   group.  
	*/

	public function createGroup($userlogInId,$groupName,$numArray)
	{
		$this->setDate();
		
		$userlogInId = $this->cleanInputs($userlogInId);
		$groupName 	 = $this->cleanInputs($groupName);
		$numArray 	 = $this->cleanInputs($numArray);

		if($this->userRole == 'Admin')
		{
			$colArray 	 = array('groupadminId','groupName','groupNumbers','groupCreatedTime');

			foreach ($numArray as $number)
			{
				$valArray 	= array($userlogInId,$groupName,$number,$this->today);
				$columns  	= array('groupId','groupadminId','groupName','groupNumbers');
				$colKeys   	= array('groupadminId','groupName','groupNumbers');
				$colvalues  = array($userlogInId,$groupName,$number);

				$query   	= $this->where($columns,TableNames['group'],$colKeys,$colvalues);	

				$res 		= $this->getSingleRow($query);
				$groupId 	= $res['groupId'];
				$result 	= $this->exists($query);
				
				if($result == true)
				{
					$updateQry = "UPDATE"." ".TableNames['group']." "."SET groupUpdatedTime = '$this->today' WHERE groupId = '$groupId' AND groupadminId = '$userlogInId' AND groupName = '$groupName' AND groupNumbers = '$number';";
					// echo "$updateQry<br><br>";
					$res = $this->execute($updateQry); 

					return ($res == TRUE) ? $this->successMessage('group details updated') : $this->errorMesaage('error in updating group details'); 
				}
				elseif($result == false)
				{
					$res = $this->Insert($colArray,$valArray,TableNames['group']);

					return ($res == TRUE) ? $this->successMessage('group details Inserted') : $this->errorMesaage('error in inserting group details'); 
				}
			}
		}
		else
		{
			$colArray 	 = array('groupUserlogInId','groupName','groupNumbers','groupCreatedTime');

			// $valArray = array($userlogInId,$groupName,implode('\',\'', $numArray));
			foreach ($numArray as $number)
			{
				$valArray 	= array($userlogInId,$groupName,$number,$this->today);

				$columns  	= array('groupId','groupUserlogInId','groupName','groupNumbers');
				$colKeys   	= array('groupUserlogInId','groupName','groupNumbers');
				$colvalues  = array($userlogInId,$groupName,$number);

				$query   	= $this->where($columns,TableNames['group'],$colKeys,$colvalues);			
				// echo "$query<br><br>";
				// $res = $this->execute($query);
				$res 		= $this->getSingleRow($query);
				$groupId 	= $res['groupId'];
				$result 	= $this->exists($query);

				if($result == true)
				{
					$updateQry = "UPDATE"." ".TableNames['group']." "."SET groupUpdatedTime = '$this->today' WHERE groupId = '$groupId' AND groupUserlogInId = '$userlogInId' AND groupName = '$groupName' AND groupNumbers = '$number';";
					// echo "$updateQry<br><br>";
					$res = $this->execute($updateQry); 

					return ($res == TRUE) ? $this->successMessage('group details updated') : $this->errorMesaage('error in updating group details'); 
				}
				elseif($result == false)
				{
					$res = $this->Insert($colArray,$valArray,TableNames['group']);

					return ($res == TRUE) ? $this->successMessage('group details inserted') : $this->errorMesaage('error in inserting group details'); 
				}			
			}
		}				
	}

	/*
	* getUserSpecificGroupDetails : To get the user specific group details.
	* Param $userlogInId is mandatory to pass.
	*/

	public function getUserSpecificGroupDetails($userlogInId)
	{
		// $this->setDate();
		$userlogInId = $this->cleanInputs($userlogInId);

		if($this->userRole == 'Admin')
		{
			$selectQry = "SELECT a.adminusername,a.adminMobile,b.groupName,b.groupNumbers,b.groupCreatedTime,b.groupUpdatedTime,b.groupStatus FROM"." ".TableNames['Admin']." "."a"." "."INNER JOIN"." ".TableNames['group']." "."b ON a.adminId = b.groupadminId WHERE b.groupadminId = '$userlogInId' GROUP BY a.adminusername,a.adminMobile,b.groupName,b.groupNumbers,b.groupCreatedTime,b.groupUpdatedTime,b.groupStatus ORDER BY a.adminusername asc";

			// echo "$selectQry";

			$row 	= $this->execute($selectQry);

			$result = $this->fetchArray($row);

			return $result;
		}
		else
		{
			$selectQry = "SELECT a.username,a.userEmail,b.groupName,b.groupNumbers,b.groupCreatedTime,b.groupUpdatedTime,b.groupStatus FROM"." ".TableNames['User']." "."a"." "."INNER JOIN"." ".TableNames['group']." "."b ON a.userlogInId = b.groupUserlogInId WHERE b.groupUserlogInId = '$userlogInId' GROUP BY a.username,a.userEmail,b.groupName,b.groupNumbers,b.groupCreatedTime,b.groupUpdatedTime,b.groupStatus ORDER BY a.username asc";

			// echo "$selectQry";

			$row 	= $this->execute($selectQry);

			$result = $this->fetchArray($row);

			return $result;
		}
	}

	/*
	* getAllGroupDetails : To fetch all the details of existing group for either Admin |
	* User.
	*/

	public function getAllGroupDetails()
	{
		if($this->userRole == 'Admin')
		{
			$selectQry = "SELECT a.adminusername,a.adminMobile,b.groupName,b.groupNumbers,b.groupCreatedTime,b.groupUpdatedTime,b.groupStatus FROM"." ".TableNames['Admin']." "."a"." "."INNER JOIN"." ".TableNames['group']." "."b ON a.adminId = b.groupadminId GROUP BY a.adminusername,a.adminMobile,b.groupName,b.groupNumbers,b.groupCreatedTime,b.groupUpdatedTime,b.groupStatus ORDER BY a.adminusername asc";
			
			$row 	= $this->execute($selectQry);

			$result = $this->fetchArray($row);

			return $result;
		}
		else
		{
			$selectQry = "SELECT a.username,a.userEmail,b.groupName,b.groupNumbers,b.groupCreatedTime,b.groupUpdatedTime,b.groupStatus FROM"." ".TableNames['User']." "."a"." "."INNER JOIN"." ".TableNames['group']." "."b ON a.userlogInId = b.groupUserlogInId GROUP BY a.username,a.userEmail,b.groupName,b.groupNumbers,b.groupCreatedTime,b.groupUpdatedTime,b.groupStatus ORDER BY a.username asc";
			
			$row 	= $this->execute($selectQry);

			$result = $this->fetchArray($row);

			return $result;
		}		
	}

	/*
	* renameGroup : To rename an existing group.
	* All the params are mandatory.
	*/

	public function renameGroup($userlogInId,$oldGroupName,$newGroupName)
	{	
		$this->setDate();
		$userlogInId 	 = $this->cleanInputs($userlogInId);
		$oldGroupName 	 = $this->cleanInputs($oldGroupName);
		$newGroupName 	 = $this->cleanInputs($newGroupName);

		if($this->userRole == 'Admin')
		{
			$updateQry = "UPDATE"." ".TableNames['group']." "."SET groupName = '$newGroupName', groupUpdatedTime = '$this->today' WHERE groupName = '$oldGroupName' AND groupadminId = '$userlogInId'";

			// echo "$updateQry";

			$result = $this->execute($updateQry);

			return ($result == TRUE) ? $this->successMessage("Renamed succesfully") : $this->errorMesaage("Server is busy Unable to process the request now. Try again later"); 
		}
		else
		{
			$updateQry = "UPDATE"." ".TableNames['group']." "."SET groupName = '$newGroupName', groupUpdatedTime = '$this->today' WHERE groupName = '$oldGroupName' AND groupUserlogInId = '$userlogInId'";

			$result = $this->execute($updateQry);

			return ($result == TRUE) ? $this->successMessage("Renamed succesfully") : $this->errorMesaage("Server is busy Unable to process the request now. Try again later"); 
		}
	}

	/*
	* deactivateGroup : To set a group in a deactivated mode for any specific user.
	* param $userlogInId is mandatory.
	*/

	public function deactivateGroup($userlogInId)
	{
		$this->setDate();
		$userlogInId = $this->cleanInputs($userlogInId);

		if($this->userRole == 'Admin')
		{
			$updateQry = "UPDATE"." ".TableNames['group']." "."SET groupStatus = '0', groupUpdatedTime = '$this->today' WHERE groupadminId = '$userlogInId'";

			$result = $this->execute($updateQry);

			return ($result == TRUE) ? $this->successMessage("Deactivated succesfully") : $this->errorMesaage("Server is busy Unable to process the request now. Try again later"); 
		}
		else
		{
			$updateQry = "UPDATE"." ".TableNames['group']." "."SET groupStatus = '0', groupUpdatedTime = '$this->today' WHERE groupUserlogInId = '$userlogInId'";

			$result = $this->execute($updateQry);

			return ($result == TRUE) ? $this->successMessage("Deactivated succesfully") : $this->errorMesaage("Server is busy Unable to process the request now. Try again later"); 
		}
	}

	/*
	* activateGroup : To set a group in a activated mode back for any specific user.
	* param $userlogInId is mandatory.
	*/

	public function activateGroup($userlogInId)
	{
		$this->setDate();
		$userlogInId = $this->cleanInputs($userlogInId);

		if($this->userRole == 'Admin')
		{
			$updateQry = "UPDATE"." ".TableNames['group']." "."SET groupStatus = '1', groupUpdatedTime = '$this->today' WHERE groupadminId = '$userlogInId'";

			$result = $this->execute($updateQry);

			return ($result == TRUE) ? $this->successMessage("Activated succesfully") : $this->errorMesaage("Server is busy Unable to process the request now. Try again later"); 
		}
		else
		{
			$updateQry = "UPDATE"." ".TableNames['group']." "."SET groupStatus = '1', groupUpdatedTime = '$this->today' WHERE groupUserlogInId = '$userlogInId'";

			$result = $this->execute($updateQry);

			return ($result == TRUE) ? $this->successMessage("Activated succesfully") : $this->errorMesaage("Server is busy Unable to process the request now. Try again later"); 
		}
	}
	public function deleteGroup($userlogInId,$groupName)
	{
		$userlogInId = $this->cleanInputs($userlogInId);
		$groupName 	 = $this->cleanInputs($groupName);

		if($this->userRole == 'Admin')
		{
			$query = "DELETE FROM"." ".TableNames['group']." "."WHERE groupadminId = '$userlogInId' AND groupName = '$groupName'";
			$result = $this->execute($query);

			return ($result == TRUE) ? $this->successMessage("Deleted succesfully") : $this->errorMesaage("Server is busy Unable to process the request now. Try again later"); 
		}
		else
		{
			$query = "DELETE FROM"." ".TableNames['group']." "."WHERE groupUserlogInId = '$userlogInId' AND groupName = '$groupName'";
			$result = $this->execute($query);

			return ($result == TRUE) ? $this->successMessage("Deleted succesfully") : $this->errorMesaage("Server is busy Unable to process the request now. Try again later"); 
		}		
	}

	/*public function deleteNum($userlogInId,$groupName,$numArray)
	{
		
	}*/
}
?>
