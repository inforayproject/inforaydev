<?php
/*
* Author 	: ravi@inforay.co
* Date 		: 2nd Jan 2017
* Purpose 	: To handle transactions for Admin as well as for Users. 
*/

error_reporting(E_ALL);
set_time_limit(0);

/*
* 	To add the common functinalities as well as the authentication functions.
*/ 

require_once '../irUtility/Authenticate.php';

/*
*	Class definition: To encapsulate the credit variables and functions together in
*	order to use these functionalities for transactions of credits(Actual / Virtual) 
*	respective to Users / Admins.	
*
*	var $userRole : Private variable accesible within the class scope only. And We are 
* 	forcing it to be Admin as the concept behind is that only an Admin can use the 
*	credit part.
*
*	__construct($role) : callling the constructor the of the class & base class and 
* 	 passing $role as param to the constuctor the set the Role(Forcing the role to be an
*	 Admin) .
*	  
*	CheckAccessByIP: this function will strict the access of the code to IP level as 
*	well as it will also allow to strict on the basis of https authentication.
*	
*	Note: Please check the class Authenticate for more explanation of CheckAccessByIP(). 
*/

class creditClass extends Authenticate
{
	private $userRole;

	public function __construct($role)
	{
		$this->userRole = $role;

		if($this->userRole != 'Admin')
		{
			$this->errorMesaage('Unauthorized access denied Please contact Admin');
			header('HTTP/1.0 404 Invalid access');
			exit;
		}
		/*elseif($this->CheckAccessByIP() == FALSE)
		{
			header('HTTP/1.0 404 Invalid Ipaddress');
			exit;
		}*/

		parent::__construct();
	}

	/*
	*	validateUser(): This function will validate the existence of the credit and 
	*	debit transaction of the user in history tables. If the transactions are 
	*	available in history tables then only it will allow to procees further otherwise 
	*	it will set the header and come out ffrom the control flow. 
	*
	* 	Params: $logInId - Login id of the user.
	*			$creditType - type of credit
	*			$transactionType - Actual / Virtual
	*
	*	All the params are mandatory(*).
	*/
	private function validateUser($logInId,$creditType,$transactionType)
	{
		/*$validate = $this->authenticateUser($logInId);*/

		if($transactionType == 'Actual')
		{
			$columns 		= array();
			$arraykey 		= array('UACHuserlogInId','UACHCTID');
			$arrayvalue 	= array($logInId,$creditType);
			$authenticate 	= $this->where($columns, TableNames['actualUserCredits'],$arraykey,$arrayvalue);
			$row 			= $this->getSingleRow($authenticate);

			return (($row['UACHuserlogInId'] == "$logInId") && ($row['UACHCTID'] == "$creditType"))? $this->json('Exists') : $this->json('user does not exists');
		}
		elseif($transactionType == 'Virtual')
		{
			$columns 		= array();
			$arraykey 		= array('UVCHuserlogInId','UVCHCTID');
			$arrayvalue 	= array($logInId,$creditType);
			$authenticate 	= $this->where($columns, TableNames['virtualUserCredits'],$arraykey,$arrayvalue);
			$row 			= $this->getSingleRow($authenticate);

			return (($row['UVCHuserlogInId'] == "$logInId") && ($row['UVCHCTID'] == "$creditType"))? $this->json('Exists') : $this->json('user does not exists');
		}
		else
		{
			return $this->errorMesaage('Unable to identify the transaction type of the user');
		}
	}

	/*
	*	validateAdmin(): This function will validate the existence of the credit and 
	*	debit transaction of the admin in history tables. If the transactions are 
	*	available in history tables then only it will allow to procees further otherwise 
	*	it will set the header and come out from the control flow. 
	*
	* 	Params: $logInId - Login id of the user.
	*			$creditType - type of credit
	*			$transactionType - Actual / Virtual
	*
	*	All the params are mandatory(*).
	*/

	private function validateAdmin($logInId,$creditType,$transactionType)
	{
		/*$validate = $this->authenticateAdmin($logInId);*/

		if($transactionType == 'Actual')
		{
			$columns 		= array();		 
			$arraykey 		= array('AACHadminId','AACHCTID');
			$arrayvalue 	= array($logInId,$creditType);
			$authenticate 	= $this->where($columns, TableNames['actualAdminCredits'],$arraykey,$arrayvalue);	

			$row 			= $this->getSingleRow($authenticate);
			
			return (($row['AACHadminId'] == "$logInId") && ($row['AACHCTID'] == "$creditType"))? $this->json('Exists') : $this->json('Admin does not exists');
		}
		elseif($transactionType == 'Virtual')
		{
			$columns 		= array();		 
			$arraykey 		= array('AVCHadminId','AVCHCTID');
			$arrayvalue 	= array($logInId,$creditType);
			$authenticate 	= $this->where($columns, TableNames['virtualAdminCredits'],$arraykey,$arrayvalue);
			$row 			= $this->getSingleRow($authenticate);

			return (($row['AVCHadminId'] == "$logInId") && ($row['AVCHCTID'] == "$creditType"))? $this->json('Exists') : $this->json('Admin does not exists');
		}
		else
		{
			return $this->errorMesaage('Unable to identify the transaction type of the Admin');
		}
	}

	/*	getTransactionId(): this will return the transactionid(Primary key) from the 
	* 	actual tables for admin and user.
	*  	Params:  $logInId - Login id of the user.
	*			 $creditType - type of credit.
	*			 $userType - Admin / User.
	*/

	private function getTransactionId($logInId,$creditType,$userType)
	{
		$columns 		= array();
		$arrayValue 	= array($logInId,$creditType);

		if($userType == 'Admin')
		{
			$arrayKey 		= array('AACadminId','AACCTID');
			$query 			= $this->where($columns, TableNames['Admincredits'],$arrayKey,$arrayValue);
			$row			= $this->getSingleRow($query);
			$transactionId 	= $row['AACtransactionID'];	

			return $transactionId;
		}
		elseif($userType == 'User')
		{
			$arrayKey 		= array('UACuserlogInId','UACCTID');
			$query 			= $this->where($columns, TableNames['Usercredits'],$arrayKey,$arrayValue);
			$row			= $this->getSingleRow($query);
			$transactionId 	= $row['UACtransactionID'];	
			
			return $transactionId; 
		}
		else
		{
			return $this->errorMesaage('Status :3005 Unable to identify the type of user');
		}
	}

	private function insertUserAvailableCredits($userLogInId,$creditType,$creditAmount,$transactionType)
	{
		$this->setDate();
		$transactionType	= $this->cleanInputs($transactionType);
		$columns 			= array();
		$arraykey 			= array('UACuserlogInId','UACCTID');
		$arrayvalue 		= array($userLogInId,$creditType);
		$selectQry 			= $this->where($columns,TableNames['Usercredits'],$arraykey,$arrayvalue);
		$row				= $this->getSingleRow($selectQry);
		$transactionID		= $row['UACtransactionID'];
		$credit 			= $row['UACactualCredit'];
		$vCredit 			= $row['UACvirtualCredit'];

		if($transactionType == 'Actual')
		{
			if(($row['UACuserlogInId'] == "$userLogInId") && ($row['UACCTID'] == "$creditType"))
			{
				$totCredit = $creditAmount + $credit;

				$updateQry = "UPDATE"." ".TableNames['Usercredits']." "."SET UACactualCredit = '$totCredit',UACupdatedTime = '$this->today' WHERE UACtransactionID = '$transactionID' AND UACuserlogInId = '$userLogInId' AND UACCTID = '$creditType'";

				$result = $this->execute($updateQry);

				return $result;
			}
			else
			{
				$colArray = array('UACuserlogInId','UACCTID','UACactualCredit','UACcreatedTime');

				$valArray = array($userLogInId,$creditType,$creditAmount,$this->today);

				$result = $this->Insert($colArray, $valArray, TableNames['Usercredits']);

				return $result;
			}
		}
		elseif($transactionType == 'Virtual')
		{
			if(($row['UACuserlogInId'] == "$userLogInId") && ($row['UACCTID'] == "$creditType"))
			{
				$totCredit = $creditAmount + $vCredit;

				$updateQry = "UPDATE"." ".TableNames['Usercredits']." "."SET UACvirtualCredit = '$totCredit',UACupdatedTime = '$this->today' WHERE UACtransactionID = '$transactionID' AND UACuserlogInId = '$userLogInId' AND UACCTID = '$creditType'";

				$result = $this->execute($updateQry);

				return $result;
			}
			else
			{
				$colArray = array('UACuserlogInId','UACCTID','UACvirtualCredit','UACcreatedTime');

				$valArray = array($userLogInId,$creditType,$creditAmount,$this->today);

				$result = $this->Insert($colArray, $valArray, TableNames['Usercredits']);

				return $result;
			}
		}
		else
		{
			return $this->errorMesaage('Unable to identify the transaction type of the user');
		}
	}

	public function userActualCreditHistory($userLogInId,$creditType,$actualCredit,$creditedBY,$ipaddress)
	{
		$authenticate = json_decode($this->authenticateUser($userLogInId));

		if($authenticate == 'Valid User')
		{
			$this->setDate();
			$userLogInId		= $this->cleanInputs($userLogInId);
			$creditType			= $this->cleanInputs($creditType);
			$actualCredit		= $this->cleanInputs($actualCredit);
			$creditedBY			= $this->cleanInputs($creditedBY);
			$ipaddress			= $this->cleanInputs($ipaddress);

			$colArray = array('UACHuserlogInId','UACHCTID','UACHcredit','UACHcreditedTime','UACHcreaditedBy','UACHloggedInIp');

			$valArray = array($userLogInId,$creditType,$actualCredit,$this->today,$creditedBY,$ipaddress);

			$result = $this->Insert($colArray, $valArray, TableNames['actualUserCredits']);

			$lastInsertedId = $this->getLastInsertedId($result);

			$validate = json_decode($this->validateUser($userLogInId,$creditType,'Actual'));

			if($validate == 'Exists')
			{
				$insertQry 		= $this->insertUserAvailableCredits($userLogInId,$creditType,$actualCredit,'Actual');

				$transactionId 	= $this->getTransactionId($userLogInId,$creditType,'User');

				$updateQry = "UPDATE"." ".TableNames['actualUserCredits']." "."SET transactionID = '$transactionId' WHERE UACHuserlogInId = '$userLogInId' AND UACHCTID = '$creditType'";

				$this->execute($updateQry);

				return (($result == TRUE) && ($insertQry == TRUE)) ? $this->successMessage("Status Code 3001: Inserted succesfully") : $this->deleteLastInsertedRow(TableNames['actualUserCredits'],'UACHtransactionID',$lastInsertedId);
			}
			else
			{
				$deleterow = $this->deleteLastInsertedRow(TableNames['actualUserCredits'],'UACHtransactionID',$lastInsertedId);

				return ($deleterow == True) ? $this->successMessage('deleted succesfully') : $this->errorMesaage('unable to delete the data');
			}
			$deleterow = $this->deleteLastInsertedRow(TableNames['actualUserCredits'],'UACHtransactionID',$lastInsertedId);			

			return ($deleterow == True) ? $this->successMessage('deleted succesfully') : $this->errorMesaage('unable to delete the data');
		}
		else
		{
			return $this->errorMesaage("Status Code 3002: $authenticate");
		}
	}

	public function userVirtualCreditHistory($userLogInId,$creditType,$virtualCredit,$creditedBY,$ipaddress)
	{
		$authenticate = json_decode($this->authenticateUser($userLogInId));

		if($authenticate == 'Valid User')
		{
			$this->setDate();
			$userLogInId		= $this->cleanInputs($userLogInId);
			$creditType			= $this->cleanInputs($creditType);
			$virtualCredit		= $this->cleanInputs($virtualCredit);
			$creditedBY			= $this->cleanInputs($creditedBY);
			$ipaddress			= $this->cleanInputs($ipaddress);

			$colArray = array('UVCHuserlogInId','UVCHCTID','UVCHcredit','UVCHcreditedTime','UVCHcreditedBy','UVCHloggedInIp');
			$valArray = array($userLogInId,$creditType,$virtualCredit,$this->today,$creditedBY,$ipaddress);

			$result   = $this->Insert($colArray, $valArray, TableNames['virtualUserCredits']);

			$lastInsertedId = $this->getLastInsertedId($result);

			$validate 		= json_decode($this->validateUser($userLogInId,$creditType,'Virtual'));

			if($validate == 'Exists')
			{
				$insertQry 		= $this->insertUserAvailableCredits($userLogInId,$creditType,$virtualCredit,'Virtual');
				$transactionId 	= $this->getTransactionId($userLogInId,$creditType,'User');

				$updateQry = "UPDATE"." ".TableNames['virtualUserCredits']." "."SET transactionID = '$transactionId' WHERE UVCHuserlogInId = '$userLogInId' AND UVCHCTID = '$creditType'";

				$this->execute($updateQry);

				return (($result == TRUE) && ($insertQry == TRUE)) ? $this->successMessage("Status Code 3003: Inserted succesfully") : $this->deleteLastInsertedRow(TableNames['virtualUserCredits'],'UVCHtransactionID',$lastInsertedId);
			}
			else
			{
				$deleterow = $this->deleteLastInsertedRow(TableNames['virtualUserCredits'],'UVCHtransactionID',$lastInsertedId);			

				return ($deleterow == True) ? $this->successMessage('deleted succesfully') : $this->errorMesaage('unable to delete the data');
			}
			$deleterow = $this->deleteLastInsertedRow(TableNames['virtualUserCredits'],'UVCHtransactionID',$lastInsertedId);			

			return ($deleterow == True) ? $this->successMessage('deleted succesfully') : $this->errorMesaage('unable to delete the data');
		}
		else
		{
			return $this->errorMesaage("Status Code 3004: $authenticate");
		}
	}

	public function userActualDebitTransactions($userLogInId,$creditType,$debitedAmount,$userType)
	{
		$this->setDate();
		$userLogInId 		= $this->cleanInputs($userLogInId);
		$creditType  		= $this->cleanInputs($creditType);
		$debitedAmount 		= $this->cleanInputs($debitedAmount);
		$userType 			= $this->cleanInputs($userType);

		$transactionID 		= $this->getTransactionId($userLogInId,$creditType,$userType);

		if($transactionID != '' && $userType == 'User')
		{
			$selectQry = "SELECT UACactualCredit,UACactualDebit FROM"." ".TableNames['Usercredits']." "."WHERE UACtransactionID = '$transactionID' AND UACuserlogInId = '$userLogInId' AND UACCTID = '$creditType'";

			$result   = $this->getSingleRow($selectQry);

			$actualCredit = $result['UACactualCredit'];
			$actualDebit  = $result['UACactualDebit'];			

			if($actualCredit >= $debitedAmount && $actualCredit != 0)
			{
				$totalActualDebit  = $debitedAmount + $actualDebit;

				$totalActualCredit = $actualCredit -  $debitedAmount;

				$updateQry = "UPDATE"." ".TableNames['Usercredits']." "."SET UACactualCredit = '$totalActualCredit',UACactualDebit = '$totalActualDebit',UACupdatedTime = '$this->today' WHERE UACtransactionID = '$transactionID' AND UACuserlogInId = '$userLogInId' AND UACCTID = '$creditType'";

				$this->execute($updateQry);

				$result = $this->successMessage("Debit updated succesfully.");

				return $result;
			}
			else
			{
				return $this->errorMesaage('Status:3009 not enough credit is available in account to make transactions');
			}				
		}
		elseif($transactionID != '' && $userType == 'Admin')
		{
			$selectQry = "SELECT AACactualCredit,AACactualDebit FROM"." ".TableNames['Admincredits']." "."WHERE AACtransactionID = '$transactionID' AND AACadminId = '$userLogInId' AND AACCTID = '$creditType'";

			$result   = $this->getSingleRow($selectQry);

			$actualCredit = $result['AACactualCredit'];
			$actualDebit  = $result['AACactualDebit'];

			if($actualCredit >= $debitedAmount && $actualCredit != 0)
			{
				$totalActualDebit  = $debitedAmount + $actualDebit;

				$totalActualCredit = $actualCredit -  $debitedAmount;

				$updateQry = "UPDATE"." ".TableNames['Admincredits']." "."SET AACactualCredit = '$totalActualCredit',AACactualDebit = '$totalActualDebit',AACupdatedTime = '$this->today' WHERE AACtransactionID = '$transactionID' AND AACadminId = '$userLogInId' AND AACCTID = '$creditType'";

				$this->execute($updateQry);

				$result = $this->successMessage("Debit updated succesfully.");

				return $result;
			}
			else
			{
				return $this->errorMesaage('Status:3010 not enough credit is available in account to make transactions');
			}	
		}
		else
		{
			return $this->errorMesaage('Status:3006 Type of User does not exists');
		}
	}

	public function userVirtualDebitTransactions($userLogInId,$creditType,$debitedAmount,$userType)
	{
		$this->setDate();
		$userLogInId 		= $this->cleanInputs($userLogInId);
		$creditType  		= $this->cleanInputs($creditType);
		$debitedAmount 		= $this->cleanInputs($debitedAmount);
		$userType 			= $this->cleanInputs($userType);

		$transactionID = $this->getTransactionId($userLogInId,$creditType,$userType);

		if($transactionID != '' && $userType == 'User')
		{
			$selectQry = "SELECT UACactualCredit,UACactualDebit,UACvirtualCredit,UACvirtualDebit FROM"." ".TableNames['Usercredits']." "."WHERE UACtransactionID = '$transactionID' AND UACuserlogInId = '$userLogInId' AND UACCTID = '$creditType'";

			$result   = $this->getSingleRow($selectQry);

			$actualCredit  = $result['UACactualCredit'];
			$actualDebit   = $result['UACactualDebit'];
			$virtualCredit = $result['UACvirtualCredit'];
			$virtualDebit  = $result['UACvirtualDebit'];			

			if($actualCredit >= $debitedAmount && $actualCredit != 0)
			{
				$totalActualDebit   = $debitedAmount + $actualDebit;

				$totalActualCredit  = $actualCredit -  $debitedAmount;

				$totalVirtualDebit  = $debitedAmount + $virtualDebit;

				$totalVirtualCredit = $virtualCredit -  $debitedAmount;

				$updateQry = "UPDATE"." ".TableNames['Usercredits']." "."SET UACactualCredit = '$totalActualCredit',UACactualDebit = '$totalActualDebit',UACvirtualCredit = '$totalVirtualCredit',UACvirtualDebit = '$totalVirtualDebit',UACupdatedTime = '$this->today' WHERE UACtransactionID = '$transactionID' AND UACuserlogInId = '$userLogInId' AND UACCTID = '$creditType'";

				$this->execute($updateQry);

				$result = $this->successMessage("Debit updated succesfully.");

				return $result;
			}
			else
			{
				return $this->errorMesaage('Status:3011 not enough credit is available in account to make transactions');
			}				
		}
		elseif($transactionID != '' && $userType == 'Admin')
		{
			$selectQry = "SELECT AACactualCredit,AACactualDebit,AACvirtualCredit,AACvirtualDebit FROM"." ".TableNames['Admincredits']." "."WHERE AACtransactionID = '$transactionID' AND AACadminId = '$userLogInId' AND AACCTID = '$creditType'";

			$result   = $this->getSingleRow($selectQry);

			$actualCredit  = $result['AACactualCredit'];
			$actualDebit   = $result['AACactualDebit'];	
			$virtualCredit = $result['AACvirtualCredit'];
			$virtualDebit  = $result['AACvirtualDebit'];

			if($actualCredit >= $debitedAmount && $actualCredit != 0)
			{
				$totalActualDebit   = $debitedAmount + $actualDebit;

				$totalActualCredit  = $actualCredit -  $debitedAmount;

				$totalVirtualDebit  = $debitedAmount + $virtualDebit;

				$totalVirtualCredit = $virtualCredit -  $debitedAmount;

				$updateQry = "UPDATE"." ".TableNames['Admincredits']." "."SET AACactualCredit = '$totalActualCredit',AACactualDebit = '$totalActualDebit',AACvirtualCredit = '$totalActualCredit',AACvirtualDebit = '$totalActualDebit',AACupdatedTime = '$this->today' WHERE AACtransactionID = '$transactionID' AND AACadminId = '$userLogInId' AND AACCTID = '$creditType'";

				$this->execute($updateQry);

				$result = $this->successMessage("Virtual Debit updated succesfully.");

				return $result;
			}
			else
			{
				return $this->errorMesaage('Status:3012 not enough credit is available in account to make transactions');
			}	
		}
		else
		{
			return $this->errorMesaage('Status:3013 Type of User does not exists');
		}
	}

	private function insertAdminAvailableCredits($adminLogInId,$creditType,$creditAmount,$transactionType)
	{
		$this->setDate();
		$transactionType	= $this->cleanInputs($transactionType);
		$columns 			= array();
		$arraykey 			= array('AACadminId','AACCTID');
		$arrayvalue 		= array($adminLogInId,$creditType);
		$selectQry 			= $this->where($columns,TableNames['Admincredits'],$arraykey,$arrayvalue);
		$row				= $this->getSingleRow($selectQry);
		$transactionID		= $row['AACtransactionID'];
		$credit 			= $row['AACactualCredit'];
		$vCredit 			= $row['AACvirtualCredit'];

		if($transactionType == 'Actual')
		{
			if(($row['AACadminId'] == "$adminLogInId") && ($row['AACCTID'] == "$creditType"))
			{
				$totCredit = $creditAmount + $credit;

				$updateQry = "UPDATE"." ".TableNames['Admincredits']." "."SET AACactualCredit = '$totCredit',AACupdatedTime = '$this->today' WHERE AACtransactionID = '$transactionID' AND AACadminId = '$adminLogInId' AND AACCTID = '$creditType'";

				$result = $this->execute($updateQry);

				return $result;
			}
			else
			{
				$colArray = array('AACadminId','AACCTID','AACactualCredit','AACcreatedTime');

				$valArray = array($adminLogInId,$creditType,$creditAmount,$this->today);

				$result = $this->Insert($colArray, $valArray, TableNames['Admincredits']);

				return $result;
			}
		}
		elseif($transactionType == 'Virtual')
		{
			if(($row['AACadminId'] == "$adminLogInId") && ($row['AACCTID'] == "$creditType"))
			{
				$totCredit = $creditAmount + $vCredit;

				$updateQry = "UPDATE"." ".TableNames['Admincredits']." "."SET AACvirtualCredit = '$totCredit',AACupdatedTime = '$this->today' WHERE AACtransactionID = '$transactionID' AND AACadminId = '$adminLogInId' AND AACCTID = '$creditType'";

				$result = $this->execute($updateQry);

				return $result;
			}
			else
			{
				$colArray = array('AACadminId','AACCTID','AACvirtualCredit','AACupdatedTime');

				$valArray = array($adminLogInId,$creditType,$creditAmount,$this->today);

				$result = $this->Insert($colArray, $valArray, TableNames['Admincredits']);

				return $result;
			}
		}
		else
		{
			return $this->errorMesaage('Unable to identify the transaction type of the admin');
		}
	}

	public function adminActualCreditHistory($adminLogInId,$creditType,$actualCredit,$creditedBY,$ipaddress)
	{
		$authenticate = json_decode($this->authenticateAdmin($adminLogInId));
		echo "$authenticate<br>";

		if($authenticate == 'Valid Admin')
		{
			$this->setDate();
			$adminLogInId		= $this->cleanInputs($adminLogInId);
			$creditType			= $this->cleanInputs($creditType);
			$actualCredit		= $this->cleanInputs($actualCredit);
			$creditedBY			= $this->cleanInputs($creditedBY);
			$ipaddress			= $this->cleanInputs($ipaddress);

			$colArray = array('AACHadminId','AACHCTID','AACHcredit','AACHcreditedTime','AACHcreditedBy','AACHloggedInIp');

			$valArray = array($adminLogInId,$creditType,$actualCredit,$this->today,$creditedBY,$ipaddress);

			$result = $this->Insert($colArray, $valArray, TableNames['actualAdminCredits']);

			$lastInsertedId = $this->getLastInsertedId($result);

			$validate = json_decode($this->validateAdmin($adminLogInId,$creditType,'Actual'));
			echo "$validate<br>";

			if($validate == 'Exists')
			{
				$insertQry 		= $this->insertAdminAvailableCredits($adminLogInId,$creditType,$actualCredit,'Actual');

				$transactionId 	= $this->getTransactionId($adminLogInId,$creditType,'Admin');

				$updateQry = "UPDATE"." ".TableNames['actualAdminCredits']." "."SET transactionID = '$transactionId' WHERE AACHadminId = '$adminLogInId' AND AACHCTID = '$creditType'";

				$this->execute($updateQry);

				return (($result == TRUE) && ($insertQry == TRUE)) ? $this->successMessage("Status Code 3001: Inserted succesfully") : $this->deleteLastInsertedRow(TableNames['actualAdminCredits'],'AACHtransactionID',$lastInsertedId);
			}
			else
			{
				$deleterow = $this->deleteLastInsertedRow(TableNames['actualAdminCredits'],'AACHtransactionID',$lastInsertedId);			

				return ($deleterow == True) ? $this->successMessage('deleted succesfully') : $this->errorMesaage('unable to delete the data');
			}
			$deleterow = $this->deleteLastInsertedRow(TableNames['actualAdminCredits'],'AACHtransactionID',$lastInsertedId);			

			return ($deleterow == True) ? $this->successMessage('deleted succesfully') : $this->errorMesaage('unable to delete the data');
		}
		else
		{
			return $this->errorMesaage("Status Code 3007: $authenticate");
		}
	}

	public function adminVirtualCreditHistory($adminLogInId,$creditType,$virtualCredit,$creditedBY,$ipaddress)
	{
		$authenticate = json_decode($this->authenticateAdmin($adminLogInId));

		if($authenticate == 'Valid Admin')
		{
			$this->setDate();
			$adminLogInId		= $this->cleanInputs($adminLogInId);
			$creditType			= $this->cleanInputs($creditType);
			$virtualCredit		= $this->cleanInputs($virtualCredit);
			$creditedBY			= $this->cleanInputs($creditedBY);
			$ipaddress			= $this->cleanInputs($ipaddress);

			$colArray = array('AVCHadminId','AVCHCTID','AVCHcredit','AVCHcreditedTime','AVCHcreditedBy','AVCHloggedInIp');

			$valArray = array($adminLogInId,$creditType,$virtualCredit,$this->today,$creditedBY,$ipaddress);

			$result   = $this->Insert($colArray, $valArray, TableNames['virtualAdminCredits']);

			$lastInsertedId = $this->getLastInsertedId($result);

			$validate 		= json_decode($this->validateAdmin($adminLogInId,$creditType,'Virtual'));

			if($validate == 'Exists')
			{
				$insertQry 		= $this->insertAdminAvailableCredits($adminLogInId,$creditType,$virtualCredit,'Virtual');

				$transactionId 	= $this->getTransactionId($adminLogInId,$creditType,'Admin');

				$updateQry = "UPDATE"." ".TableNames['virtualAdminCredits']." "."SET transactionID = '$transactionId' WHERE AVCHadminId = '$adminLogInId' AND AVCHCTID = '$creditType'";

				$this->execute($updateQry);

				return (($result == TRUE) && ($insertQry == TRUE)) ? $this->successMessage("Status Code 3003: Inserted succesfully") : $this->deleteLastInsertedRow(TableNames['virtualUserCredits'],'UVCHtransactionID',$lastInsertedId);		
			}
			else
			{
				$deleterow = $this->deleteLastInsertedRow(TableNames['virtualAdminCredits'],'AVCHtransactionID',$lastInsertedId);			

				return ($deleterow == True) ? $this->successMessage('deleted succesfully') : $this->errorMesaage('unable to delete the data');
			}
			$deleterow = $this->deleteLastInsertedRow(TableNames['virtualAdminCredits'],'AVCHtransactionID',$lastInsertedId);			

			return ($deleterow == True) ? $this->successMessage('deleted succesfully') : $this->errorMesaage('unable to delete the data');
		}
		else
		{
			return $this->errorMesaage("Status Code 3008: $authenticate");
		}
	}
	public function getAllUsersAvailableCredits()
	{
		$selectQry = "SELECT * FROM"." ".TableNames['Usercredits'];

		$result = $this->execute($selectQry);

		return $this->fetchArray($result);
	}
	public function getSpecificUserAvailableCredits($userLogInId)
	{
		$userLogInId = $this->cleanInputs($userLogInId);

		$selectQry = "SELECT * FROM"." ".TableNames['Usercredits']." "."WHERE UACuserlogInId = '$userLogInId'";

		$result = $this->execute($selectQry);

		return $this->fetchArray($result);
	}
	public function getAllAdminAvailableCredits()
	{
		$selectQry = "SELECT * FROM"." ".TableNames['Admincredits'];

		$result = $this->execute($selectQry);

		return $this->fetchArray($result);
	} 
	public function getSpecificAdminAvailableCredits($adminLogInId)
	{
		$adminLogInId = $this->cleanInputs($adminLogInId);

		$selectQry = "SELECT * FROM"." ".TableNames['Admincredits']." "."WHERE AACadminId = '$adminLogInId'";

		$result = $this->execute($selectQry);

		return $this->fetchArray($result);
	}
	public function getUserActualCreditHistory($userLogInId,$creditType=FALSE)
	{
		
	}
	public function getAdminActualcreditHistory($adminLogInId,$creditType=FALSE)
	{

	}
	public function getUserVirtualCreditHistory($userLogInId,$creditType=FALSE)
	{

	}
	public function getAdminvirtualCreditHistory($adminLogInId,$creditType=FALSE)
	{
		
	}
}

?>
