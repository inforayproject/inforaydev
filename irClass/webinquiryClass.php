<?php
/*
* Author 	: ravi@inforay.co
* Date 		: 24th Nov 2016
* Purpose 	: To get the inquiry details of the user. 
*/

error_reporting(E_ALL);
set_time_limit(0);

/*
* 	To add the common functinalities.
*/  
require_once '../irUtility/Common.php';

/*
*	Class to implement the inquiry functionalities with respect to the user.
* 	Role is mandatory but by default this class will only be used by an admin as inquiry * 	 details is for sales guys to get the lead.* 	
*/

class webinquiryClass extends Common
{
	/*
	* 	To set the role of the logged in user.
	*/ 

	private $userRole;

	/*
	* 	Always pass the Admin in the constructor while creating an object of this class.
	*/

	public function __construct($role)
	{
		$this->userRole = $role;
		parent::__construct();
	}

	/*
	* 	To insert the inquired data collected || posted on the webpage of the company by * 	 new interested individuals.
	*
	* 	Params to pass with the functions.
	*
	* 	$uname -> necessary param to pass. It will hold the inquired Person Name as 
	*	posted through contact or inquiry form.
	*
	* 	$phone -> necessary To hold contact Num so that our sales representative can 
	* 	contact the person.
	* 
	* 	$email ->  Required as it will hold the email of the user so that in future we 
	*   can contact the person through email as well.
	*
	*	$ipaddres -> for security purpose we are storing the ip and it is also mandatory.
	*/

	public function insertWIData($uname,$phone,$email,$ipaddress)
	{
		$this->setDate();

		$uname 		= $this->cleanInputs($uname);
		$phone 		= $this->cleanInputs($phone);
		$email 		= $this->cleanInputs($email);
		$ipaddress  = $this->cleanInputs($ipaddress);

		if(filter_var($email, FILTER_VALIDATE_EMAIL) === false)
		{
			/*$errmsg = "Invalid email Format";
			return $errmsg;*/
			if($this->userRole == 'Admin')
			{
				exit('Backtrace: Invalid email Format passed in'.'<br><br> line no: '.__LINE__.'<br><br> file path: '.__FILE__.'<br><br> Function Name: '. __FUNCTION__.'<br><br> of Class:'.__CLASS__);			
	
			}
			else
			{
				exit('Invalid email Format. Please provide a valid email id');
			}
			
		}
		elseif(filter_var($ipaddress, FILTER_VALIDATE_IP) === false)
		{
			exit('Invalid ip Format');
		}
		else
		{
			$colArray = array('WIname','WIphoneNo','WIemail','WIloggedInIp','WIrequestedTime');
			$valArray = array($uname,$phone,$email,$ipaddress,$this->today);

			$res = $this->Insert($colArray,$valArray,TableNames['inquiry']);

			return ($res == TRUE) ? $this->successMessage("Inserted succesfully") : $this->errorMesaage("Server is busy Unable to process the request now. Try again later");			
		}		
	}

	//To get all the data from the table.
	 
	public function getWIData()
	{	
		$array = array('WID','WIname','WIphoneNo','WIemail','WIloggedInIp','WIrequestedTime');
		$data = $this->Select($array,TableNames['inquiry']);

		$result = $this->fetchArray($data);
		return $result;   
	}

	//To Notify with daily inquired users reports to the relevant persons through mail.
	/*public function sendMail()
	{
		$this->setDate();
		$array = array('WID','name','phoneNo','email','loggedInIp','requestedTime');
		$data = $this->Select($array,'irWebInquiry');

		$caption = "WebInquiry Report for ($this->fromdate to $this->todate)";

		$html_data = <<<EOT
		<html>
			<body>
			<table  align="center" style='width:100%;' border="1">
		EOT;
		$html_data .= "<tr><th colspan='3'>WebInquiry Report for ($this->fromdate to $this->todate)</th></tr>";	
		$html_data .= "<tr>";
		$html_data .= "<td>User Name</td><td>Contact Num</td><td>Email</td><td>Ipaddress</td><td>Requested Time</td>";
		$html_data .= "</tr>";
		while($row = $data->fetch_assoc())
		{
			$html_data .= "<tr><td style=text-align: centre;>".$row['name']."</td><td style=text-align: centre;>".$row['phoneNo']."</td><td style=text-align: centre;>".$row['email']."</td><td style=text-align: centre;>".$row['loggedInIp']."</td><td style=text-align: centre;>".$row['requestedTime']."</td></tr>";
		}
		echo "</tbody></table>";
		$html_data .= <<<EOT
		</table>
		</body>
		</html> 
		EOT;
	}*/
}
?>
