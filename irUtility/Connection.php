<?php
/*
* Date   : 28th Nov 2016
* Author : Arun Billur | Ravi Ranjan
*/
class Connection

{
   private $servername  = "localhost";
   private $username    = "root";
   private $password    = "root";
   private $dbname      = "infoRayDevelopment";
   private $con;

   /*header('Access-Control-Allow-Origin: *');
   header('Access-Control-Allow-Methods: GET, POST');
   header("Access-Control-Allow-Headers: X-Requested-With");*/
   
   public function __construct()
   {
      $this->con = new mysqli($this->servername, $this->username, $this->password, $this->dbname, 3306);
      if ($this->con->connect_error)
      {
         die("conection error: " . $this->con->conect_error);
      }

      /*else
      {
      echo "connection successful <br />";
      }  */
      define('TableNames', array(
         'userLogIn'          => 'userLogInHistory',
         'Admin'              => 'irAdminLogIn',
         'group'              => 'irGroup',
         'User'               => 'irUserLogIn',
         'Usercredits'        => 'irUserAvailableCredits',
         'Admincredits'       => 'irAdminAvailableCredits',
         'actualUserCredits'  => 'irUserActualCreditHistory',
         'virtualUserCredits' => 'irUserVirtualCreditHistory',
         'actualAdminCredits'  => 'irAdminActualCreditHistory',
         'virtualAdminCredits' => 'irAdminVirtualCreditHistory',
         'blacklist'          => 'irBlacklist',
         'inquiry'            => 'irWebInquiry'
      ));
   }

   public function __destruct()
   {
      // $this->con->close();
      unset($this->con);
   }

   public function Select($array = FALSE, $table)
   {

      // Please pass an empty array if needed to select all the rows from a table.
      if (empty($array))
      {
         $query  = "SELECT * FROM $table";
         $result = $this->execute($query);
         return $result;
      }
      else
      {
         $columns = implode(",", $array);

         // $columns = "'"."$column"."'";

         $query  = "SELECT $columns FROM $table";
         $result = $this->execute($query);
         return $result;
      }
   }

   public function Insert($colArray, $valArray, $table)
   {
      if (!empty($colArray) && !empty($valArray) && !empty($table))
      {
         $columns = implode(",", $colArray);

         // $values = implode(",",$valArray);

         $row = "";
         foreach($valArray as $val)
         {
            $row.= "'$val'" . ",";
         }

         $values = trim($row, ",");
         
         $query = "INSERT INTO $table ($columns) VALUES ($values);";

         // echo "$query<br /><br />";

         $result = $this->execute($query);

         return $result;
      }
      else
      {
         $errmsg = "Missing / empty argument for function Insert()";
         return $errmsg;
         exit();
      }
   }

   /*public function Delete($array,$table)
   {
   }

   public function Update($array,$table)
   {
   }*/
   public function Drop($table)
   {
      $query = "DROP TABLE $table";
      $this->execute($query);
      exit;
   }

   public function Truncate($table)
   {
      $query = "TRUNCATE TABLE $table";
      $this->execute($query);
      exit;
   }

   public function getNumRows($query)
   {
      $num_rows = $this->execute($query);
      $result = $num_rows->num_rows;
      return $result;
   }

   public function getSingleRow($query)
   {
      $row = $this->execute($query);
      $result = $row->fetch_assoc();

      // print_r($result);

      return $result;
   }

   public function execute($query)
   {
      $result = $this->con->query($query);
      return $result;
   }

   public function getLastInsertedId($param = FALSE)
   {
      return ($param == TRUE) ? $this->con->insert_id : 'FALSE';
   }

   public function exists($query)
   {
      $num_rows = $this->execute($query);
      $result = $num_rows->num_rows;
      if ($result > 0)
      {
         return true;
      }
      else
      {
         return false;
      }
   }

   public function executeMultiQuery($query)
   {
      $result = $this->con->multi_query($query);
      return $result;
   }

   public function where($columns = FALSE, $table, $arraykey = FALSE, $arrayvalue = FALSE, $limit = FALSE)
   {

      // pass two empty array if need this functionalities else please use Select() || execute().

      if (empty($arraykey) && empty($arrayvalue))
      {
         $query = "SELECT * FROM $table";

         // $result=$this->execute($query);

         return $query;
      }
      else
      {
         if (sizeof($arraykey) == sizeof($arrayvalue))
         {
            if (empty($columns))
            {

               // $columns = implode(",",$arraykey);
               // $data = implode(",",$arrayvalue);
               // $columns = "'"."$column"."'";

               $row = "";
               foreach($arraykey as $key => $value)
               {
                  $row.= $value . "=" . '\'' . $arrayvalue[$key] . '\'' . " " . "AND" . " ";
               }

               $row = rtrim($row, "AND ");
               $query = "SELECT * FROM $table WHERE $row";
               
               // $result=$this->execute($query);

               return $query;
            }
            else
            {
               $columns = implode(",", $columns);

               // $data = implode(",",$arrayvalue);
               // $columns = "'"."$column"."'";

               $row = "";
               foreach($arraykey as $key => $value)
               {
                  $row.= $value . "=" . '\'' . $arrayvalue[$key] . '\'' . " " . "AND" . " ";
               }

               $row = rtrim($row, "AND ");
               $query = "SELECT $columns FROM $table WHERE $row;";

               // echo "$query";
               // $result=$this->execute($query);

               return $query;
            }
         }
         else
         {
            $msg = "Array keys and array values should be equal";
            return $msg;
         }
      }
   }
   public function deleteLastInsertedRow($table,$column,$lastInsertedId)
   {
      $query = "DELETE FROM $table WHERE $column = '$lastInsertedId'";
      echo "$query";
      $result = $this->execute($query);
      return $result;
   }
}