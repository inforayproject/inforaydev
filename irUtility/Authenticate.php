<?php
/*
* Date 		: 10th Dec 2016
* Author 	: Ravi Ranjan
*/
require_once 'Common.php';

class Authenticate extends Common

{
	public function __construct()
	{
		parent::__construct();
	}

	/*
	*  This function returns True if visitor IP is allowed.
	*  Otherwise it returns False
	*/
	protected function CheckAccessByIP()
	{

		// allowed IP. Change it to your static IP

		$allowedip = array(
			'127.0.0.1'
		);

		// $ip = $_SERVER['REMOTE_ADDR'];

		$this->CheckAccessByHTTP();

		$ip = $this->getClientIp();
		foreach($allowedip as $ipaddress)
		{
			return ($ip == $ipaddress);
		}		
	}

	protected function CheckAccessByHTTP()
	{
		$valid_passwords = array(
			"ravi" => "ranjan",
			"arun" => "billur",
			"midhun" => "haridas"
		);
		$valid_users = array_keys($valid_passwords);
		$user = @$_SERVER['PHP_AUTH_USER'];
		$pass = @$_SERVER['PHP_AUTH_PW'];
		if (!isset($_SESSION['firstauthenticate']))
		{
			session_start();
		}		

		if (!isset($_SERVER['PHP_AUTH_USER']) || strcmp($_SERVER['PHP_AUTH_USER'], $user) != 0 || !isset($_SERVER['PHP_AUTH_PW']) || strcmp($_SERVER['PHP_AUTH_PW'], $pass) != 0 || !isset($_SESSION['firstauthenticate']) || !$_SESSION['firstauthenticate'])
		{
			$_SESSION['firstauthenticate'] = true;
			$this->setAuthenticateHeaders();
		}
		else
		{
			// I destroy the session var now

			session_unset();

			// Your code below
		}
	}

	protected function setAuthenticateHeaders()
	{
		header('WWW-Authenticate: Basic realm="Sistema autentificaci�n UnoAutoSur"');
		header('HTTP/1_0 401 Unauthorized');

		// header("Status: 401 Access Denied");
		// $this->errorMesaage('Unauthorized access terminating script');

		echo $this->json('Unauthorized access terminating script Please login to continue');
		exit;
	}
	protected function authenticateUser($logInId)
	{
		$logInId = $this->cleanInputs($logInId);
		// $columns 	= array('userlogInId','role','status');
		$columns 	= array();		 
		$arraykey 	= array('userlogInId');
		$arrayvalue = array($logInId);
		$validate 	= $this->where($columns, TableNames['User'],$arraykey,$arrayvalue);
		
		$row 		= $this->getSingleRow($validate);
		
		$status = $row['status'];
		
		return ($status == 'active')? $this->json('Valid User') : $this->json('Invalid User');
	}
	protected function authenticateAdmin($logInId)
	{
		$logInId = $this->cleanInputs($logInId);

		// $columns 	= array('adminId','role','status');
		$columns 	= array();		 
		$arraykey 	= array('adminId');
		$arrayvalue = array($logInId);
		$validate 	= $this->where($columns, TableNames['Admin'],$arraykey,$arrayvalue);
		$row 		= $this->getSingleRow($validate);
		
		return ($row['status'] == 'active')? $this->json('Valid Admin') : $this->json('Invalid admin');
	}
}


?>